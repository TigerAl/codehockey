import debug.DebugInfo;
import model.*;

import static java.lang.StrictMath.PI;

public final class MyStrategy implements Strategy {
    @Override
    public void move(Hockeyist self, World world, Game game, Move move) {
        DebugInfo debugInfo = DebugController.getDebugInfo();
        try {
            doMove(self, world, game, move, debugInfo);
        } catch (Exception e) {
            debugInfo.exception = e;
        }
    }

    public void doMove(Hockeyist self, World world, Game game, Move move, DebugInfo debugInfo) {
        move.setSpeedUp(-1.0D);
        move.setTurn(PI);
        move.setAction(ActionType.STRIKE);
        if (world.getTick() == 200) {
            throw new RuntimeException("Just emulate some exception during strategy processing.");
        }
    }
}
