import model.Game;
import model.Hockeyist;
import model.Move;
import model.PlayerContext;

import java.io.IOException;
import java.net.ConnectException;

public final class Runner {
    private final RemoteProcessClient remoteProcessClient;
    private final String token;
    private boolean showDebugWindow;

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length == 3) {
            new Runner(args).run();
        } else {
            new Runner(new String[]{"127.0.0.1", "31001", "0000000000000000"}).run();
        }
    }

    private Runner(String[] args) throws IOException, InterruptedException {
        RemoteProcessClient remoteProcessClient;
        if (System.getProperty("startLocalRunner") != null ) {
            // start local-runner server
            Thread localRunnerThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        LocalTestRunner.main(new String[] {"tools/local-runner.properties"});
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            localRunnerThread.start();

            // start Runner client. If local-runner is not started yet then wait a bit and retry.
            while (true) {
                try {
                    remoteProcessClient = new RemoteProcessClient(args[0], Integer.parseInt(args[1]));
                    break;
                } catch (ConnectException e) {
                    System.out.println(e.getMessage());
                    synchronized (this) {
                        this.wait(500);
                    }
                }
            }

        } else {
            remoteProcessClient = new RemoteProcessClient(args[0], Integer.parseInt(args[1]));
        }
        showDebugWindow = System.getProperty("showDebugWindow") != null;
        this.remoteProcessClient = remoteProcessClient;
        token = args[2];
    }

    public void run() throws IOException {
        try {
            remoteProcessClient.writeToken(token);
            int teamSize = remoteProcessClient.readTeamSize();
            remoteProcessClient.writeProtocolVersion();
            Game game = remoteProcessClient.readGameContext();

            Strategy[] strategies = new Strategy[teamSize];

            for (int strategyIndex = 0; strategyIndex < teamSize; ++strategyIndex) {
                if (showDebugWindow) {
                    strategies[strategyIndex] = DebugController.getInstance().createNewStrategy();
                } else {
                    strategies[strategyIndex] = new MyStrategy();
                }
            }

            PlayerContext playerContext;

            while ((playerContext = remoteProcessClient.readPlayerContext()) != null) {
                Hockeyist[] playerHockeyists = playerContext.getHockeyists();
                if (playerHockeyists == null || playerHockeyists.length != teamSize) {
                    break;
                }

                Move[] moves = new Move[teamSize];

                for (int hockeyistIndex = 0; hockeyistIndex < teamSize; ++hockeyistIndex) {
                    Hockeyist playerHockeyist = playerHockeyists[hockeyistIndex];

                    Move move = new Move();
                    moves[hockeyistIndex] = move;
                    strategies[playerHockeyist.getTeammateIndex()].move(
                            playerHockeyist, playerContext.getWorld(), game, move
                    );
                }

                remoteProcessClient.writeMoves(moves);
            }
        } finally {
            remoteProcessClient.close();
        }
    }
}
