package debug;

import model.*;

public class HistoryRecord {
    private Hockeyist hockeyist;
    private World world;
    private Game game;
    private Move move;
    private DebugInfo debugInfo;

    public HistoryRecord(Hockeyist hockeyist, World world, Game game, Move move, DebugInfo debugInfo) {
        this.hockeyist = hockeyist;
        this.world = world;
        this.game = game;
        this.move = move;
        this.debugInfo = debugInfo;
    }

    public World getWorld() {
        return world;
    }

    public Hockeyist getHockeyist() {
        return hockeyist;
    }

    public Game getGame() {
        return game;
    }

    public Move getMove() {
        return move;
    }

    public DebugInfo getDebugInfo() {
        return debugInfo;
    }
}
