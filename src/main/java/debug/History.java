package debug;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import model.*;
import static debug.HistoryTimeLineMarker.MarkerType.*;

public class History {

    private static final int INITIAL_HISTORY_SIZE = 6000;

    private int currentTick;

    private List<Map<Integer, HistoryRecord>> historyRecordList;

    private List<HistoryTimeLineMarker> timeLineMarkers;

    private HistoryTimeLineMarker currentPeriod;

    public History() {
        historyRecordList = new ArrayList<>(INITIAL_HISTORY_SIZE);
        currentTick = 0;
        timeLineMarkers = Collections.synchronizedList(new LinkedList<HistoryTimeLineMarker>());
        currentPeriod = new HistoryTimeLineMarker(GAME_PERIOD, currentTick);
        timeLineMarkers.add(currentPeriod);
    }

    public Map<Integer, HistoryRecord> getCurrentRecords() {
        return historyRecordList.get(currentTick);
    }

    public Map<Integer, HistoryRecord> getRecordsByTick(int tickNumber) {
        return historyRecordList.get(tickNumber);
    }

    public int getCurrentTick() {
        return currentTick;
    }

    public void setCurrentTick(int tickNumber) {
        if (tickNumber < 0) {
            currentTick = 0;
        } else if (tickNumber > getLastTick()) {
            currentTick = getLastTick();
        } else {
            currentTick = tickNumber;
        }
    }

    public int getLastTick() {
        return historyRecordList.size() - 1;
    }

    public int getMaxTickValue() {
        return Math.max(INITIAL_HISTORY_SIZE, getSize());
    }

    public int getSize() {
        return historyRecordList.size();
    }

    public List<HistoryTimeLineMarker> getTimeLineMarkers() {
        return timeLineMarkers;
    }

    /**
     * Add new tick history information.
     *
     * @param historyRecords set of HistoryRecord object, map key is teammate index.
     */
    public void addRecords(Map<Integer, HistoryRecord> historyRecords) {
        historyRecordList.add(historyRecords);
    }

    public void updateTimeLineMarkers () {
        // check if current period is end
        int lastTick = getLastTick();
        Map<Integer, HistoryRecord> historyRecords = getRecordsByTick(lastTick);
        World curWorld = historyRecords.values().iterator().next().getWorld();
        if (currentPeriod.getMarkerType() == GAME_PERIOD) {
            if (curWorld.getMyPlayer().isJustMissedGoal()) {
                currentPeriod.setMarkerType(LOSE_PERIOD);
                currentPeriod.setEndIndex(lastTick - 1);
                currentPeriod = new HistoryTimeLineMarker(OUT_OF_GAME_TIME, lastTick);
                timeLineMarkers.add(currentPeriod);
            } else if (curWorld.getMyPlayer().isJustScoredGoal()) {
                currentPeriod.setMarkerType(WON_PERIOD);
                currentPeriod.setEndIndex(lastTick - 1);
                currentPeriod = new HistoryTimeLineMarker(OUT_OF_GAME_TIME, lastTick);
                timeLineMarkers.add(currentPeriod);
            } else {
                currentPeriod.setEndIndex(lastTick);
            }
        }

        // check if out of game period is end
        if (currentPeriod.getMarkerType() == OUT_OF_GAME_TIME) {
            if (!curWorld.getMyPlayer().isJustMissedGoal() && !curWorld.getMyPlayer().isJustScoredGoal()) {
                currentPeriod.setEndIndex(lastTick - 1);
                currentPeriod = new HistoryTimeLineMarker(GAME_PERIOD, lastTick);
                timeLineMarkers.add(currentPeriod);
            } else {
                currentPeriod.setEndIndex(lastTick);
            }
        }

        // check if any of strategies was crashed during last tick calculation
        for (HistoryRecord historyRecord : historyRecords.values()) {
            if (historyRecord.getDebugInfo().exception != null) {
                HistoryTimeLineMarker crashedMarker = new HistoryTimeLineMarker(STRATEGY_CRASHED, lastTick);
                crashedMarker.setEndIndex(lastTick);
                timeLineMarkers.add(crashedMarker);
            }
        }
    }
}
