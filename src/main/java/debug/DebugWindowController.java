package debug;

import java.util.Map;

import model.Game;

public interface DebugWindowController {

    /**
     * Stop/start game/replay animation.
     *
     * @param isPaused true to pause, false to resume.
     */
    void switchPauseState(boolean isPaused);

    /**
     * Move current history position to particular tick. Do not pause/resume game animation.
     *
     * @param tickIndex tick index to navigate within history log.
     *                  If tickIndex < 0 or tickIndex > history.getLastTick() then it will be truncated.
     */
    void setCurrentTickIndex(int tickIndex);

    /**
     * Call MyStrategy.move(...) method in order to debug code lines with IDE tools.
     */
    void recalculateCurrentTick();

    /**
     * Provide an access to read a DebugController.history object.
     *
     * @return History object instance.
     */
    History getHistory();

    /**
     * Provide an access to read a DebugController.game object.
     *
     * @return History object instance.
     */
    Game getGame();

    /**
     * Select/deselect particular teammate unit in order to show additional info or to recalculate particular strategy.
     *
     * @param selectedTeammateIndex teammate index [0, 1, ...] to select teammate or null to undo selection.
     */
    void setSelectedTeammateIndex(Integer selectedTeammateIndex);

    /**
     * @return currently selected teammate index or null if no one currently selected.
     */
    Integer getSelectedTeammateIndex();

    /**
     * Provide a HistoryRecord for current tick and for currently selected teammate.
     * Should be used only if teammate is selected,
     *
     * @return HistoryRecord object or null if no HistoryRecord is available.
     */
    HistoryRecord getCurrentHistoryRecordForSelectedTeammate();

    /**
     * Provide a collection of HistoryRecord for current tick. Usually, the size of this collection is equal to teamSize.
     *
     * @return collection of HistoryRecord for current tick or empty collection is no records are available.
     */
    Map<Integer, HistoryRecord> getHistoryRecordsForCurrentTick();


    boolean isPaused();
}
