package debug;

public interface DebugWindowView {

    public void repaintAll();

    public void repaintHistoryTimeLine();
}
