package debug;

public class HistoryTimeLineMarker {
    private MarkerType markerType;
    private int startIndex;
    private int endIndex;

    public HistoryTimeLineMarker(MarkerType markerType, int startIndex) {
        this.markerType = markerType;
        this.startIndex = startIndex;
    }

    public MarkerType getMarkerType() {
        return markerType;
    }

    public void setMarkerType(MarkerType markerType) {
        this.markerType = markerType;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public enum MarkerType {
        GAME_PERIOD,
        OUT_OF_GAME_TIME,
        STRATEGY_CRASHED,
        WON_PERIOD,
        LOSE_PERIOD
    }
}
