package debug;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Map;

import model.*;
import static debug.HistoryTimeLineMarker.MarkerType;

public class DebugWindow extends JFrame implements DebugWindowView {

    private static final int TIME_LINE_CONTROL_HEIGHT = 20;
    private static final int GAME_FIELD_OFFSET_X = 10;
    private static final int GAME_FIELD_OFFSET_Y = -130;

    private DebugWindowController controller;
    private Image gameFieldLayer;
    private Image unitsLayer;
    private Image timeLineLayer;
    private Image textInfoLayer;
    private Image bufferLayer;
    private boolean isTimeLineDragging;
    private boolean isPausedBeforeDragging;

    public DebugWindow(final DebugWindowController controller) {
        super("Debug window by TigerAl");
        this.controller = controller;
        this.setSize(1050, 650);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.setContentPane(new JPanel() {
            @Override
            public void paint(Graphics g) {
                // if game was not started yet then show splash screen
                if (gameFieldLayer == null) {
                    g.drawString("game is still loading...", 0, 0);
                } else {
                    Graphics2D buf = (Graphics2D) bufferLayer.getGraphics();
                    buf.setBackground(this.getBackground());
                    buf.clearRect(0, 0, getWidth(), getHeight());

                    buf.drawImage(gameFieldLayer, GAME_FIELD_OFFSET_X, GAME_FIELD_OFFSET_Y, null);
                    buf.drawImage(unitsLayer, GAME_FIELD_OFFSET_X, GAME_FIELD_OFFSET_Y, null);
                    buf.drawImage(timeLineLayer, 0, 0, null);
                    buf.drawImage(textInfoLayer, 0, 0, null);
                    g.drawImage(bufferLayer, 0, 0, null);
                }
            }
        });

        // change mouse cursor on hover
        this.getContentPane().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                Container contentPane = DebugWindow.this.getContentPane();
                if (e.getY() > contentPane.getHeight() - TIME_LINE_CONTROL_HEIGHT) {
                    contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                } else {
                    contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                }
            }
        });

        // handle click on play/pause button
        this.getContentPane().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Container contentPane = DebugWindow.this.getContentPane();
                if (e.getY() > contentPane.getHeight() - TIME_LINE_CONTROL_HEIGHT
                        && e.getX() < TIME_LINE_CONTROL_HEIGHT) {
                    controller.switchPauseState(!controller.isPaused());
                }
            }
        });

        // handle click on time line and time line cursor drag
        this.getContentPane().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1 && isMousePosWithinTimeLine(e.getX(), e.getY())) {
                    controller.setCurrentTickIndex(getTickByMousePosWithinTimeLine(e.getX(), e.getY()));
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1 && isMousePosWithinTimeLine(e.getX(), e.getY())) {
                    isTimeLineDragging = true;
                    isPausedBeforeDragging = controller.isPaused();
                    controller.switchPauseState(true);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1 && isTimeLineDragging) {
                    isTimeLineDragging = false;
                    controller.switchPauseState(isPausedBeforeDragging);
                }
            }
        });
        this.getContentPane().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (isTimeLineDragging) {
                    controller.setCurrentTickIndex(getTickByMousePosWithinTimeLine(e.getX(), e.getY()));
                }
            }
        });

        // handle space button press to recalculate MyStrategy,move method.
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_SPACE && controller.isPaused()) {
                    controller.recalculateCurrentTick();
                }
            }
        });

        // handle hockeyist selection/deselection
        this.getContentPane().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1 && !isMousePosWithinTimeLine(e.getX(), e.getY())) {
                    World world = getWorldForCurrentTick();
                    if (world != null) {
                        for (Hockeyist hockeyist : world.getHockeyists()) {
                            if (hockeyist.isTeammate()) {
                                double dX = e.getX() - GAME_FIELD_OFFSET_X - hockeyist.getX();
                                double dY = e.getY() - GAME_FIELD_OFFSET_Y - hockeyist.getY();
                                if (dX*dX + dY*dY < hockeyist.getRadius()*hockeyist.getRadius()) {
                                    controller.setSelectedTeammateIndex(hockeyist.getTeammateIndex());
                                    return;
                                }
                            }
                        }
                    }
                    else {
                        controller.setSelectedTeammateIndex(null);
                    }
                }
            }
        });

        this.setVisible(true);
    }

    @Override
    public void repaintAll() {
        if (gameFieldLayer == null) {
            initializeLayers();
            drawGameField();
        }
        drawUnits();
        repaintHistoryTimeLine();
        drawTextInfo();
        getContentPane().repaint();
    }

    @Override
    public void repaintHistoryTimeLine() {
        if (timeLineLayer != null) {
            drawTimeLine();
            getContentPane().repaint();
        }
    }

    public void drawTimeLine() {
        Graphics2D g = (Graphics2D) timeLineLayer.getGraphics();

        // clear image
        g.setBackground(new Color(255, 255, 255, 0));
        g.clearRect(0, 0, this.getContentPane().getWidth(), this.getContentPane().getWidth());

        int navBarHeight = TIME_LINE_CONTROL_HEIGHT;
        int navBarWidth = this.getContentPane().getWidth() - navBarHeight;
        int navBarTop = this.getContentPane().getHeight() - navBarHeight;
        int navBarLeft = this.getContentPane().getWidth() - navBarWidth;

        // draw play/pause button
        g.setColor(Color.BLACK);
        g.setStroke(new BasicStroke(3));
        if (controller.isPaused()) {
            g.drawPolygon(new int[]{5, 15, 5}, new int[]{navBarTop + 5, navBarTop + 10, navBarTop + 15}, 3);
        } else {
            g.drawLine(7, navBarTop + 5, 7, navBarTop + navBarHeight - 5);
            g.drawLine(13, navBarTop + 5, 13, navBarTop + navBarHeight - 5);
        }

        // draw current ticks calculation progress
        History history = controller.getHistory();
//        int sectionWidth = history.getLastTick() * navBarWidth / history.getMaxTickValue();
//        g.setColor(Color.GREEN);
//        g.fillRect(navBarLeft, navBarTop, sectionWidth, navBarHeight);

        // draw time line markers
        for (HistoryTimeLineMarker marker : history.getTimeLineMarkers()) {
            if (marker.getMarkerType() == MarkerType.STRATEGY_CRASHED) {
                g.setStroke(new BasicStroke(3));
                g.setColor(Color.RED);
                int markerX = navBarLeft + marker.getStartIndex() * navBarWidth / history.getMaxTickValue();
                g.drawLine(markerX, navBarTop, markerX, navBarTop + navBarHeight);
            } else if (marker.getMarkerType() == MarkerType.LOSE_PERIOD
                    || marker.getMarkerType() == MarkerType.WON_PERIOD
                    || marker.getMarkerType() == MarkerType.GAME_PERIOD
                    || marker.getMarkerType() == MarkerType.OUT_OF_GAME_TIME) {
                // determine marker color
                switch (marker.getMarkerType()) {
                    case WON_PERIOD:
                        g.setColor(new Color(0, 0, 255, 150));
                        break;
                    case LOSE_PERIOD:
                        g.setColor(new Color(255, 100, 100, 150));
                        break;
                    case OUT_OF_GAME_TIME:
                        g.setColor(new Color(255, 255, 100, 150));
                        break;
                    case GAME_PERIOD:
                        g.setColor(new Color(0, 255, 0, 150));
                        break;
                    default:
                        continue;
                }
                // draw marker on time line
                int markerStartX = navBarLeft + marker.getStartIndex() * navBarWidth / history.getMaxTickValue();
                int markerEndX = navBarLeft + marker.getEndIndex() * navBarWidth / history.getMaxTickValue();
                g.fillRect(markerStartX, navBarTop, markerEndX - markerStartX, navBarHeight);
            }
        }

        // draw time line border
        g.setColor(Color.BLACK);
        g.drawRect(navBarLeft, navBarTop, navBarWidth, navBarHeight);

        // draw time line cursor
        int currentTickX = navBarLeft + history.getCurrentTick() * navBarWidth / history.getMaxTickValue();
        g.setStroke(new BasicStroke(3));
        g.drawLine(currentTickX, navBarTop, currentTickX, navBarTop + navBarHeight);
    }

    private void drawGameField() {
        Graphics2D g = (Graphics2D) gameFieldLayer.getGraphics();
        Game game = controller.getGame();

        g.setColor(Color.BLACK);
        g.drawLine((int) game.getRinkLeft(), (int) game.getRinkTop(),
                (int) game.getRinkRight(), (int) game.getRinkTop());
        g.drawLine((int) game.getRinkRight(), (int) game.getRinkTop(),
                (int) game.getRinkRight(), (int) game.getGoalNetTop());
        g.drawLine((int) game.getRinkRight(), (int) game.getGoalNetTop(),
                (int) (game.getRinkRight() + game.getGoalNetWidth()), (int) game.getGoalNetTop());
        g.drawLine((int) (game.getRinkRight() + game.getGoalNetWidth()), (int) game.getGoalNetTop(),
                (int) (game.getRinkRight() + game.getGoalNetWidth()), (int) (game.getGoalNetTop() + game.getGoalNetHeight()));
        g.drawLine((int) (game.getRinkRight() + game.getGoalNetWidth()), (int) (game.getGoalNetTop() + game.getGoalNetHeight()),
                (int) game.getRinkRight(), (int) (game.getGoalNetTop() + game.getGoalNetHeight()));
        g.drawLine((int) game.getRinkRight(), (int) (game.getGoalNetTop() + game.getGoalNetHeight()),
                (int) game.getRinkRight(), (int) game.getRinkBottom());
        g.drawLine((int) game.getRinkRight(), (int) game.getRinkBottom(),
                (int) game.getRinkLeft(), (int) game.getRinkBottom());
        g.drawLine((int) game.getRinkLeft(), (int) game.getRinkBottom(),
                (int) game.getRinkLeft(), (int) (game.getGoalNetTop() + game.getGoalNetHeight()));
        g.drawLine((int) game.getRinkLeft(), (int) (game.getGoalNetTop() + game.getGoalNetHeight()),
                (int) (game.getRinkLeft() - game.getGoalNetWidth()), (int) (game.getGoalNetTop() + game.getGoalNetHeight()));
        g.drawLine((int) (game.getRinkLeft() - game.getGoalNetWidth()), (int) (game.getGoalNetTop() + game.getGoalNetHeight()),
                (int) (game.getRinkLeft() - game.getGoalNetWidth()), (int) game.getGoalNetTop());
        g.drawLine((int) (game.getRinkLeft() - game.getGoalNetWidth()), (int) game.getGoalNetTop(),
                (int) game.getRinkLeft(), (int) game.getGoalNetTop());
        g.drawLine((int) game.getRinkLeft(), (int) game.getGoalNetTop(),
                (int) game.getRinkLeft(), (int) game.getRinkTop());
    }

    private void drawUnits() {
        Graphics2D g = (Graphics2D) unitsLayer.getGraphics();
        Game game = controller.getGame();
        World world = getWorldForCurrentTick();
        if (world == null) {
            return;
        }

        g.setBackground(new Color(255, 255, 255, 0));
        g.clearRect(0, 0, (int) game.getWorldWidth(), (int) game.getWorldHeight());


        // draw puck
        g.setColor(Color.BLACK);
        Puck puck = world.getPuck();
        g.fillOval((int) puck.getX() - (int) puck.getRadius(), (int) puck.getY() - (int) puck.getRadius(),
                (int) puck.getRadius() * 2, (int) puck.getRadius() * 2);

        // draw hockeists
        for (Hockeyist hockeyist : world.getHockeyists()) {
            int x = (int) hockeyist.getX();
            int y = (int) hockeyist.getY();
            int radius = (int) hockeyist.getRadius();

            AffineTransform at = new AffineTransform();
            at.setToRotation(hockeyist.getAngle(), x, y);
            g.setTransform(at);

            // draw countdown till hockeyist would be able to perform any action
            int arcAngle = 360 * hockeyist.getRemainingCooldownTicks() / 60;
            g.setColor(hockeyist.isTeammate() ? new Color(0, 0, 255, 50) : new Color(255, 0, 0, 50));
            g.fillArc(x - radius, y - radius, 2 * radius, 2 * radius, 90, arcAngle);

            g.setColor(hockeyist.isTeammate() ? Color.BLUE : Color.RED);
            if (hockeyist.isTeammate() && controller.getSelectedTeammateIndex() != null
                    && controller.getSelectedTeammateIndex() == hockeyist.getTeammateIndex())
            {
                g.setStroke(new BasicStroke(3));
            } else {
                g.setStroke(new BasicStroke(1));
            }

            // draw body
            g.drawOval(x - radius, y - radius, 2 * radius, 2 * radius);

            // draw eyes and mouth
            if (hockeyist.getState() == HockeyistState.KNOCKED_DOWN) {
                g.drawLine(x + 3, y - 13, x + 13, y - 3);
                g.drawLine(x + 3, y - 3, x + 13, y - 13);
                g.drawLine(x + 3, y + 13, x + 13, y + 3);
                g.drawLine(x + 3, y + 3, x + 13, y + 13);
                g.drawLine(x + 20, y - 10, x + 20, y + 10);
            } else {
                g.drawOval(x + 3, y - 13, 10, 10);
                g.drawOval(x + 3, y + 5, 10, 10);
                g.fillOval(x + 5, y - 11, 6, 6);
                g.fillOval(x + 5, y + 7, 6, 6);
                g.drawArc(x + 2, y - 10, 20, 20, -45, 90);
            }

            // draw goalie helmet
            if (hockeyist.getType() == HockeyistType.GOALIE) {
                for (int lineX = 0; lineX < radius; lineX += 10) {
                    int lineY = (int) Math.sqrt(radius * radius - lineX * lineX);
                    g.drawLine(x + lineX, y - lineY, x + lineX, y + lineY);
                }
                for (int lineY = 0; lineY < radius; lineY += 10) {
                    int lineX = (int) Math.sqrt(radius * radius - lineY * lineY);
                    g.drawLine(x, y - lineY, x + lineX, y - lineY);
                    g.drawLine(x, y + lineY, x + lineX, y + lineY);
                }
            }

            // draw stick range
            g.drawLine(x, y + 25, x + 40, y + 25);
            g.drawLine(x + 40, y + 25, x + 45, y + 10);
        }
    }

    private void drawTextInfo() {
        Graphics2D g = (Graphics2D) textInfoLayer.getGraphics();
        g.setBackground(new Color(255, 255, 255, 0));
        g.clearRect(0, 0, getWidth(), getHeight());
        World world = getWorldForCurrentTick();

        g.setColor(Color.BLACK);
        g.setFont(new Font(null, Font.PLAIN, 30));
        g.drawString(world.getOpponentPlayer().getGoalCount() + " : " + world.getMyPlayer().getGoalCount(),
                500, 30);
    }

    private void initializeLayers() {
        int gameWidth = (int) controller.getGame().getWorldWidth() + 1;
        int gameHeight = (int) controller.getGame().getWorldHeight();
        gameFieldLayer = new BufferedImage(gameWidth, gameHeight, BufferedImage.TYPE_INT_ARGB);
        unitsLayer = new BufferedImage(gameWidth, gameHeight, BufferedImage.TYPE_INT_ARGB);

        int screenWidth = this.getWidth();
        int screenHeight = this.getHeight();
        timeLineLayer = new BufferedImage(screenWidth, screenHeight, BufferedImage.TYPE_INT_ARGB);
        textInfoLayer = new BufferedImage(screenWidth, screenHeight, BufferedImage.TYPE_INT_ARGB);
        bufferLayer = new BufferedImage(screenWidth, screenHeight, BufferedImage.TYPE_INT_RGB);
    }

    private boolean isMousePosWithinTimeLine(int x, int y) {
        Container contentPane = DebugWindow.this.getContentPane();
        return y > contentPane.getHeight() - TIME_LINE_CONTROL_HEIGHT && x > TIME_LINE_CONTROL_HEIGHT;
    }

    @SuppressWarnings("unused")
    private int getTickByMousePosWithinTimeLine(int x, int y) {
        return (x - TIME_LINE_CONTROL_HEIGHT) * controller.getHistory().getMaxTickValue()
                / (DebugWindow.this.getContentPane().getWidth() - TIME_LINE_CONTROL_HEIGHT);
    }

    private World getWorldForCurrentTick() {
        if (controller.getSelectedTeammateIndex() == null) {
            Map<Integer, HistoryRecord> records = controller.getHistoryRecordsForCurrentTick();
            if (records.isEmpty()) {
                System.out.println("No history records are found for tick " + controller.getHistory().getCurrentTick());
                return null;
            }
            return records.values().iterator().next().getWorld();
        } else {
            HistoryRecord record = controller.getCurrentHistoryRecordForSelectedTeammate();
            if (record == null) {
                System.out.println(String.format("No history record are found for tick %s and teammate %s.",
                        controller.getHistory().getCurrentTick(), controller.getSelectedTeammateIndex()));
                return null;
            }
            return record.getWorld();
        }
    }
}
