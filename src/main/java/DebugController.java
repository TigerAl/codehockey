import javax.swing.AbstractAction;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import debug.*;
import model.*;

public class DebugController implements Strategy, DebugWindowController{

    private static final int FPS = 60;
    private static final int TIME_DELTA_BETWEEN_FRAMES = 1000 / FPS;
    private static DebugController debugControllerInstance;

    private DebugWindowView debugWindow;
    private List<Strategy> strategies;
    private History history;
    private Game game;
    private DebugInfo currentDebugInfoItem;

    private boolean isPaused;
    private boolean isWaitingForNewTickCalculated;
    private boolean needToRepaintHistoryTimeLine;
    private boolean needToRepaintAll;
    private Integer selectedTeammateIndex;

    public static DebugController getInstance() {
        if (debugControllerInstance == null) {
            debugControllerInstance = new DebugController();
        }
        return debugControllerInstance;
    }

    private DebugController() {
        if (System.getProperty("showDebugWindow") != null) {
            debugWindow = new DebugWindow(this);
            history = new History();
            strategies = new ArrayList<>();
            isPaused = false;
            selectedTeammateIndex = null;
//            ReplayTimer replayTimer = new ReplayTimer();
//            replayTimer.start();
            initializeTimer();
        } else {
            // this case is designed for the working on a Live mail.ru server
            currentDebugInfoItem = new DebugInfo();
        }
    }

    /**
     * Static method to get a DebugInfo object from MyStrategy class and all underlying classes.
     *
     * @return DebugInfo object for the current tick and for the current
     */
    public static DebugInfo getDebugInfo() {
        return getInstance().currentDebugInfoItem;
    }

    /**
     * Create new MyStrategy instance and return reference to DebugController instead of MyStrategy itself.
     *
     * @return DebugController instance which emulate all the MyStrategy behaviour.
     */
    public Strategy createNewStrategy() {
        Strategy strategy = new MyStrategy();
        strategies.add(strategy);
        return this;
    }

    @Override
    public void setSelectedTeammateIndex(Integer selectedTeammateIndex) {
        this.selectedTeammateIndex = selectedTeammateIndex;
        repaintAll();
    }

    @Override
    public Integer getSelectedTeammateIndex() {
        return selectedTeammateIndex;
    }

    @Override
    public void switchPauseState(boolean isPaused) {
        this.isPaused = isPaused;
    }

    @Override
    public void setCurrentTickIndex(int tickIndex) {
        history.setCurrentTick(tickIndex);
        repaintAll();
    }

    @Override
    public void recalculateCurrentTick() {
        replaySelectedHistoryRecord();
        repaintAll();
    }

    @Override
    public History getHistory() {
        return history;
    }

    @Override
    public Game getGame() {
        return game;
    }

    @Override
    public HistoryRecord getCurrentHistoryRecordForSelectedTeammate() {
        if (selectedTeammateIndex != null) {
            return history.getCurrentRecords().get(selectedTeammateIndex);
        } else {
            return null;
        }
    }

    @Override
    public Map<Integer, HistoryRecord> getHistoryRecordsForCurrentTick() {
        return history.getCurrentRecords();
    }

    @Override
    public boolean isPaused() {
        return isPaused;
    }


    /**
     * Emulate the MyStrategy.move(...) method call. Used by Runner.
     */
    @Override
    public void move(Hockeyist self, World world, Game game, Move move) {
        // initialize game object if not initialized yet
        if (this.game == null) {
            this.game = game;
        }

        strategyMove(self, world, game, move);

        // if this is a first call for currentTick then create new history item, or get existed otherwise.
        Map<Integer, HistoryRecord> recordsForCurrentTick;
        if (world.getTick() >= history.getLastTick()) {
            recordsForCurrentTick = new HashMap<>(getTeamSize());
            history.addRecords(recordsForCurrentTick);
        } else {
            recordsForCurrentTick = history.getRecordsByTick(world.getTick());
        }
        HistoryRecord newHistoryRecord = new HistoryRecord(self, world, game, move, currentDebugInfoItem);
        recordsForCurrentTick.put(self.getTeammateIndex(), newHistoryRecord);

        // update history time line on debugWindow because a new tick was fully calculated
        if (recordsForCurrentTick.size() == getTeamSize()) {
            updateViewOnNewTickCalculated();
            history.updateTimeLineMarkers();
        }
    }

    public void replaySelectedHistoryRecord() {
        Map<Integer, HistoryRecord> historyRecordsForCurrentTick = history.getCurrentRecords();
        if (selectedTeammateIndex != null) {
            HistoryRecord record = historyRecordsForCurrentTick.get(selectedTeammateIndex);
            strategyMove(record.getHockeyist(), record.getWorld(), record.getGame(), record.getMove());
        } else {
            for (HistoryRecord record : historyRecordsForCurrentTick.values()) {
                strategyMove(record.getHockeyist(), record.getWorld(), record.getGame(), record.getMove());
            }
        }
    }



    private void strategyMove(Hockeyist self, World world, Game game, Move move) {
        currentDebugInfoItem = new DebugInfo();
        Strategy currentHockeyistStrategy = strategies.get(self.getTeammateIndex());
        currentHockeyistStrategy.move(self, world, game, move);
    }

    private int getTeamSize() {
        return strategies.size();
    }

    private void updateViewOnNewTickCalculated() {
        if (!isPaused && isWaitingForNewTickCalculated) {
            setCurrentTickIndex(history.getLastTick());
        } else {
            repaintHistoryTimeLine();
        }
        isWaitingForNewTickCalculated = false;
    }

    private void initializeTimer() {
        Timer replayTimer = new Timer(TIME_DELTA_BETWEEN_FRAMES, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!isPaused) {
                    Timer replayTimer = (Timer) e.getSource();
                    // calculate the current tick value
                    int tickDelta = replayTimer.getDelay() / replayTimer.getInitialDelay();
                    History history = DebugController.this.getHistory();
                    int newTickIndex = history.getCurrentTick() + tickDelta;
                    int historyLastAvailableTick = history.getLastTick();
                    if (newTickIndex > historyLastAvailableTick) {
                        newTickIndex = historyLastAvailableTick;
                        isWaitingForNewTickCalculated = true;
                    }

                    if (newTickIndex != history.getCurrentTick() && newTickIndex > 0) {
                        DebugController.this.setCurrentTickIndex(newTickIndex);
                    }

                    if (needToRepaintAll) {
                        debugWindow.repaintAll();
                        needToRepaintAll = false;
                        needToRepaintHistoryTimeLine = false;
                    }
                    else if (needToRepaintHistoryTimeLine) {
                        debugWindow.repaintHistoryTimeLine();
                        needToRepaintHistoryTimeLine = false;
                    }
                }
            }
        });
        replayTimer.start();
    }

    private void repaintAll() {
        needToRepaintAll = true;
    }

    private void repaintHistoryTimeLine() {
        needToRepaintHistoryTimeLine = true;
    }

}
